# BB Data Fix
A plugin for local development only. Run it in your project and append `?bb_data_fix` to a url on a site. It will attempt to fix serialized data in the `wp_postmeta` table. This plugin is an unofficial helper from the BB team.

**NB** - you may need to set the memory limit to `-1` for this to work. For instance `wp config set WP_MEMORY_LIMIT -1`.