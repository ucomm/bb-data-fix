<?php

/**
 * Base64 decodes all builder data that has been Base64 
 * encoded to prevent serialized arrays from breaking 
 * when a site is being migrated.
 *
 * @since 0.2
 */
final class BB_Data_Decoder {
	
	/**
	 * @since 0.2
	 * @return void
	 */
	static public function init()
	{
		if ( ! current_user_can( 'update_core' ) ) {
			return;
		}
		if ( ! isset( $_REQUEST['bb_data_decode'] ) ) {
			return;
		}
		
		global $wpdb;
		
		$result = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE meta_key = '_fl_builder_data' OR meta_key = '_fl_builder_draft'" );
	
		foreach ( $result as $row ) {
			
			$encoded = @unserialize( $row->meta_value );
			
			if ( ! is_array( $encoded ) || ! isset( $encoded['encoded'] ) ) {
				continue;
			}
			
			$decoded = base64_decode( $encoded['encoded'] );
			
			if ( false === $decoded ) {
				continue;
			}
			
			$wpdb->update(
				$wpdb->postmeta,
				array( 'meta_value' => $decoded ),
				array( 'meta_id'	=> $row->meta_id )
			);
		}
		
		echo 'Data Decoding Complete';
		
		die();
	}
}
