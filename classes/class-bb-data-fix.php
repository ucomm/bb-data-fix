<?php

/**
 * Portions borrowed from https://github.com/Blogestudio/Fix-Serialization/blob/master/fix-serialization.php
 *
 * @since 0.1
 */
final class BB_Data_Fix {

	/**
	 * @since 0.1
	 * @return void
	 */
	static public function init()
	{
		if ( ! current_user_can( 'remove_users' ) ) {
			return;
		}
		if ( ! isset( $_REQUEST['bb_data_fix'] ) ) {
			return;
		}

		global $wpdb;

		$result = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE meta_key = '_fl_builder_data' OR meta_key = '_fl_builder_draft'" );

		foreach ( $result as $row ) {

			$fixed = self::fix_data( $row->meta_value );

			if ( $fixed != $row->meta_value ) {

				$wpdb->update(
					$wpdb->postmeta,
					array( 'meta_value' => $fixed ),
					array( 'meta_id'	=> $row->meta_id )
				);

				echo 'Fixed Meta ID: ' . $row->meta_id . "<br />";
			}
		}

		echo 'Data Fix Complete';

		die();
	}

	/**
	 * @since 0.1
	 * @access private
	 * @return string
	 */
	static private function fix_data( $data )
	{
		if ( empty( $data ) || @unserialize( $data ) !== false ) {
			return $data;
		}

		return preg_replace_callback( '/s\:(\d+)\:\"(.*?)\";/s', 'BB_Data_Fix::regex_callback', $data );
	}

	/**
	 * @since 0.1
	 * @return string
	 */
	static public function regex_callback( $matches )
	{
		return 's:' . strlen( self::unescape_mysql( $matches[2] ) ) . ':"' . self::unescape_quotes( $matches[2] ) . '";';
	}

	/**
	 * Unescape to avoid dump-text issues.
	 *
	 * @since 0.1
	 * @access private
	 * @return string
	 */
	static private function unescape_mysql($value)
	{
		return str_replace( array( "\\\\", "\\0", "\\n", "\\r", "\Z",  "\'", '\"' ),
						   array( "\\",   "\0",  "\n",  "\r",  "\x1a", "'", '"' ),
						   $value );
	}

	/**
	 * Fix strange behaviour if you have escaped quotes in your replacement.
	 *
	 * @since 0.1
	 * @access private
	 * @return string
	 */
	static private function unescape_quotes($value)
	{
		return str_replace( '\"', '"', $value );
	}
}
