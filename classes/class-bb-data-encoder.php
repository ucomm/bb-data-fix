<?php

/**
 * Base64 encodes all builder data to prevent serialized 
 * arrays from breaking when a site is being migrated.
 *
 * @since 0.2
 */
final class BB_Data_Encoder {
	
	/**
	 * @since 0.2
	 * @return void
	 */
	static public function init()
	{
		if ( ! current_user_can( 'update_core' ) ) {
			return;
		}
		if ( ! isset( $_REQUEST['bb_data_encode'] ) ) {
			return;
		}
		
		global $wpdb;
		
		$result = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE meta_key = '_fl_builder_data' OR meta_key = '_fl_builder_draft'" );
	
		foreach ( $result as $row ) {
			
			$encoded = @unserialize( $row->meta_value );
			
			if ( is_array( $encoded ) && isset( $encoded['encoded'] ) ) {
				continue;
			}
			
			$wpdb->update(
				$wpdb->postmeta,
				array( 'meta_value' => serialize( array(
					'encoded' => base64_encode( $row->meta_value )
				) ) ),
				array( 'meta_id'	=> $row->meta_id )
			);
		}
		
		echo 'Data Encoding Complete';
		
		die();
	}
}
