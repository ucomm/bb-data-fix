<?php
/**
 * Plugin Name: Beaver Builder Data Fix
 * Plugin URI: https://www.wpbeaverbuilder.com/?utm_source=external&utm_medium=bb-data-fix&utm_campaign=plugins-page
 * Description: A plugin to fix corrupted Beaver Builder data.
 * Version: 0.2
 * Author: The Beaver Builder Team
 * Author URI: https://www.wpbeaverbuilder.com/?utm_source=external&utm_medium=bb-data-fix&utm_campaign=plugins-page
 * Copyright: (c) 2015 Beaver Builder
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */
define( 'BB_DATA_FIX_VERSION', '0.2' );
define( 'BB_DATA_FIX_DIR', plugin_dir_path( __FILE__ ) );
define( 'BB_DATA_FIX_URL', plugins_url( '/', __FILE__ ) );

/* Classes */
require_once BB_DATA_FIX_DIR . 'classes/class-bb-data-decoder.php';
require_once BB_DATA_FIX_DIR . 'classes/class-bb-data-encoder.php';
require_once BB_DATA_FIX_DIR . 'classes/class-bb-data-fix.php';

/* Actions */
add_action( 'init', 'BB_Data_Decoder::init' );
add_action( 'init', 'BB_Data_Encoder::init' );
add_action( 'init', 'BB_Data_Fix::init' );